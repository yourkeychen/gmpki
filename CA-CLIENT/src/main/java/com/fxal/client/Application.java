package com.fxal.client;

import com.fxal.client.cmp.CMPClient;
import com.fxal.client.netty.CAClient;
import lombok.extern.slf4j.Slf4j;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;

import java.security.Security;

/**
 * @author: caiming
 * @Date: 2022/5/17 9:29
 * @Description:
 */
@SpringBootApplication
@EnableConfigurationProperties
@EnableCaching
@Slf4j
public class Application implements CommandLineRunner {

    @Autowired
    private CAClient caClient;

    @Autowired
    private CMPClient cmpClient;


    public static void main(String args[]) {
        Security.addProvider(new BouncyCastleProvider());
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        //caClient.connect();
        long total = 100;
        cmpClient.msgMap.clear();
        cmpClient.msgCountMap.clear();
        cmpClient.msgCountMap.put(CMPClient.MSG_SUM_KEY,total);
        cmpClient.msgCountMap.put(CMPClient.MSG_COUNT_KEY,0l);
        cmpClient.msgCountMap.put(CMPClient.MSG_BEGIN_TIME_KEY,System.currentTimeMillis());
        log.info("****************************开始测试证书申请请求，请求次数：{}个*****************************",total);
        for(int i = 0 ;i<total;i++ ){
            cmpClient.testSendCertReq();
        }
    }
}
